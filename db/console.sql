SELECT users.gender,
       users.email,
       users.phone,
       users.cell,
       users.nat,
       json_build_object(
               'title', name.title,
               'first', name.first,
               'last', name.last
           ) as name,
       json_build_object(
               'city', location.city,
               'state', location.state,
               'country', location.country,
               'postcode', location.postcode,
               'timezone', json_build_object(
                       'offset', timezone.offset,
                       'description', timezone.description
                   ),
               'street', json_build_object(
                       'number', street.number,
                       'name', street.name
                   ),
               'coordinates', json_build_object(
                       'offset', coordinates.latitude,
                       'description', coordinates.
                           longitude
                   )
           ) as location,
       json_build_object(
               'date', registered.date,
               'age', registered.age
           ) as registered,
       json_build_object(
               'large', picture.large,
               'medium', picture.medium,
               'thumbnail', picture.thumbnail
           ) as picture,
       json_build_object(
               'uuid', login.uuid,
               'username', login.username,
               'password', login.password,
               'salt', login.salt,
               'md5', login.md5,
               'sha1', login.sha1,
               'sha256', login.sha256
           ) as login,
       json_build_object(
               'name', id.name,
               'value', id.value
           ) as id,
       json_build_object(
               'date', date_of_birth.date,
               'age', date_of_birth.age
           ) as dob
FROM users
         LEFT JOIN name ON users.id = name.user_id
         LEFT JOIN location ON users.id = location.user_id
         LEFT JOIN timezone ON location.id = timezone.location_id
         LEFT JOIN coordinates ON location.id = coordinates.location_id
         LEFT JOIN street ON location.id = street.location_id
         LEFT JOIN registered ON users.id = registered.user_id
         LEFT JOIN picture ON users.id = picture.user_id
         LEFT JOIN login ON users.id = login.user_id
         LEFT JOIN id ON users.id = id.user_id
         LEFT JOIN date_of_birth ON users.id = date_of_birth.user_id
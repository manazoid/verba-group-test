create table users
(
    id     serial
        constraint users_pk
            primary key,
    gender varchar not null,
    email  varchar not null,
    phone  varchar not null,
    cell   varchar not null,
    nat    varchar not null
);

create table location
(
    city     varchar not null,
    state    varchar not null,
    country  varchar not null,
    postcode varchar not null,
    user_id  integer not null
        constraint location_users_id_fk
            references users
            on update cascade on delete cascade,
    id       serial
        constraint location_pk
            primary key
);


create table timezone
(
    "offset"    varchar not null,
    description varchar not null,
    id          serial
        constraint timezone_pk
            primary key,
    location_id integer not null
        constraint timezone_location_id_fk
            references location
            on update cascade on delete cascade
);

create table street
(
    number      integer not null,
    name        varchar not null,
    id          serial
        constraint street_pk
            primary key,
    location_id integer not null
        constraint street_location_id_fk
            references location
            on update cascade on delete cascade
);

create table registered
(
    date    timestamp with time zone not null,
    age     integer                  not null,
    id      serial
        constraint registered_pk
            primary key,
    user_id integer                  not null
        constraint registered_users_id_fk
            references users
            on update cascade on delete cascade
);

create table picture
(
    large     varchar not null,
    medium    varchar not null,
    thumbnail varchar not null,
    id        serial
        constraint picture_pk
            primary key,
    user_id   integer not null
        constraint picture_users_id_fk
            references users
            on update cascade on delete cascade
);

create table name
(
    title   varchar not null,
    first   varchar not null,
    last    varchar not null,
    id      serial
        constraint name_pk
            primary key,
    user_id integer not null
        constraint name_users_id_fk
            references users
            on update cascade on delete cascade
);

create table login
(
    uuid     varchar not null,
    username varchar not null,
    password varchar not null,
    salt     varchar not null,
    md5      varchar not null,
    sha1     varchar not null,
    sha256   varchar not null,
    id       serial
        constraint login_pk
            primary key,
    user_id  integer not null
        constraint login_users_id_fk
            references users
            on update cascade on delete cascade
);

create table id
(
    name    varchar not null,
    value   varchar not null,
    id      serial
        constraint id_pk
            primary key,
    user_id integer not null
        constraint id_users_id_fk
            references users
            on update cascade on delete cascade
);

create table date_of_birth
(
    date    timestamp with time zone not null,
    age     integer                  not null,
    id      serial
        constraint date_of_birth_pk
            primary key,
    user_id integer                  not null
        constraint date_of_birth_users_id_fk
            references users
            on update cascade on delete cascade
);
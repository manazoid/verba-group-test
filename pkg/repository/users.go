package repository

import (
	"github.com/jmoiron/sqlx"
	"verba-group-test/models"
)

type UserPostgres struct {
	db *sqlx.DB
}

func NewUserPostgres(db *sqlx.DB) *UserPostgres {
	return &UserPostgres{db: db}
}

func (r *UserPostgres) AddUser(input *models.User) error {
	// User
	user, err := r.db.Query(`INSERT INTO users (gender, email, phone, cell, nat) 
					VALUES ($1,$2,$3,$4,$5) returning id`,
		input.Gender, input.Email, input.Phone, input.Cell, input.Nationality)
	if err != nil {
		return err
	}
	defer user.Close()

	var userId int64
	if user.Next() {
		if err := user.Scan(&userId); err != nil {
			return err
		}
	}

	_, err = r.db.Exec(`INSERT INTO name (title, first, last, user_id)
				VALUES ($1,$2,$3,$4)`,
		input.Name.Title, input.Name.First, input.Name.Last, userId)
	if err != nil {
		return err
	}

	// Location
	location, err := r.db.Query(`INSERT INTO location (city, state, country, postcode, user_id)
				VALUES ($1,$2,$3,$4,$5) returning id`,
		input.Location.City, input.Location.State, input.Location.Country, input.Location.Postcode, userId)
	if err != nil {
		return err
	}
	defer location.Close()

	var locationId int64
	if location.Next() {
		if err := location.Scan(&locationId); err != nil {
			return err
		}
	}

	// Street
	_, err = r.db.Exec(`INSERT INTO street (number, name, location_id)
				VALUES ($1,$2,$3)`,
		input.Location.Street.Number, input.Location.Street.Name, locationId)
	if err != nil {
		return err
	}

	// Coordinates
	_, err = r.db.Exec(`INSERT INTO coordinates (latitude, longitude, location_id)
				VALUES ($1,$2,$3)`,
		input.Location.Coordinates.Latitude, input.Location.Coordinates.Longitude, locationId)
	if err != nil {
		return err
	}

	// Timezone
	_, err = r.db.Exec(`INSERT INTO timezone ("offset", description, location_id)
				VALUES ($1,$2,$3)`,
		input.Location.Timezone.Offset, input.Location.Timezone.Description, locationId)
	if err != nil {
		return err
	}

	// Login
	login := &input.Login
	if _, err = r.db.Exec(
		`INSERT INTO login (uuid, username, password, salt, md5, sha1, sha256, user_id)
				VALUES ($1,$2,$3,$4,$5,$6,$7,$8)`,
		login.UUID,
		login.Username,
		login.Password,
		login.Salt,
		login.MD5,
		login.SHA1,
		login.SHA256,
		userId,
	); err != nil {
		return err
	}

	// DOB
	if _, err = r.db.Exec(
		`INSERT INTO date_of_birth (date, age, user_id)
				VALUES ($1,$2,$3)`,
		input.DateOfBirth.Date, input.DateOfBirth.Age, userId); err != nil {
		return err
	}

	// Registered
	if _, err = r.db.Exec(
		`INSERT INTO registered (date, age, user_id)
				VALUES ($1,$2,$3)`,
		input.Registered.Date, input.Registered.Age, userId); err != nil {
		return err
	}

	// Identity Document
	if _, err = r.db.Exec(
		`INSERT INTO id (name, value, user_id)
				VALUES ($1,$2,$3)`,
		input.ID.Name, input.ID.Value, userId); err != nil {
		return err
	}

	// Pictures
	if _, err = r.db.Exec(
		`INSERT INTO picture (large, medium, thumbnail, user_id)
				VALUES ($1,$2,$3,$4)`,
		input.Picture.Large, input.Picture.Medium, input.Picture.Thumbnail, userId); err != nil {
		return err
	}

	return nil
}

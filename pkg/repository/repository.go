package repository

import (
	"github.com/jmoiron/sqlx"
	"verba-group-test/models"
)

type User interface {
	AddUser(input *models.User) error
}

type Repository struct {
	User User
}

func NewRepository(db *sqlx.DB) *Repository {
	return &Repository{
		User: NewUserPostgres(db),
	}
}

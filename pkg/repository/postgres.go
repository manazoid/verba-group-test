package repository

import (
	"context"
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"time"
)

type InputConfig struct {
	Host     string
	Port     string
	Username string
	Password string
	DBName   string
	SSLMode  string
}

func NewPostgresDB(conf InputConfig) (*sqlx.DB, error) {
	ctxTimeout, ctxCancel := context.WithTimeout(context.Background(), time.Second*3)
	defer ctxCancel()

	db, err := sqlx.ConnectContext(ctxTimeout, "postgres", fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		conf.Host, conf.Port, conf.Username, conf.Password, conf.DBName, conf.SSLMode))
	if err != nil {
		return nil, err
	}

	db.SetMaxIdleConns(5)
	db.SetConnMaxIdleTime(10 * time.Second)
	db.SetMaxOpenConns(95)

	return db, nil
}

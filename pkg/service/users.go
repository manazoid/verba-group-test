package service

import (
	"encoding/json"
	"io"
	"net/http"
	"verba-group-test/models"
	"verba-group-test/pkg/repository"
)

type UserService struct {
	repo repository.User
}

func NewUserService(repo repository.User) *UserService {
	return &UserService{repo: repo}
}

func (s *UserService) AddUser() error {
	// send a GET request to the external API
	resp, err := http.Get("https://randomuser.me/api/")
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// read the response body into a byte slice
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	// unmarshal the response body into a models.User struct
	var input models.Input

	if err := json.Unmarshal(body, &input); err != nil {
		return err
	}

	return s.repo.AddUser(&input.Results[0])
}

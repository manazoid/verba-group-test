package service

import (
	"verba-group-test/pkg/repository"
)

type User interface {
	AddUser() error
}

type Service struct {
	User User
}

func NewService(repos *repository.Repository) *Service {
	return &Service{
		User: NewUserService(repos.User),
	}
}

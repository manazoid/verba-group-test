package handler

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"verba-group-test/models"
)

func (h *Handler) AddUser(c *gin.Context) {
	err := h.services.User.AddUser()
	if err != nil {
		sendStatusInternalWithMessage(c, err)
		return
	}

	message := &models.SuccessOutput{
		Message: "Новый пользователь успешно добавлен",
	}

	c.JSON(http.StatusOK, message)
}

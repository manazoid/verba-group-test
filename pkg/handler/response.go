package handler

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func statusWithMessage(c *gin.Context, status int, err error) {
	var output struct {
		Message string `json:"error"`
	}
	output.Message = err.Error()
	c.JSON(status, &output)
}

func sendBadRequest(c *gin.Context) {
	c.Status(http.StatusBadRequest)
}

func sendStatusInternal(c *gin.Context) {
	c.Status(http.StatusInternalServerError)
}

func sendUnauthorizedError(c *gin.Context, err error) {
	statusWithMessage(c, http.StatusUnauthorized, err)
}

func sendStatusInternalWithMessage(c *gin.Context, err error) {
	statusWithMessage(c, http.StatusInternalServerError, err)
}

func sendBadRequestWithMessage(c *gin.Context, err error) {
	statusWithMessage(c, http.StatusBadRequest, err)
}

func sendUnprocEntityWithMessage(c *gin.Context, err error) {
	statusWithMessage(c, http.StatusUnprocessableEntity, err)
}

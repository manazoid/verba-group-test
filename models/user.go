package models

import (
	"time"
)

type (
	Input struct {
		Results []User `json:"results"`
	}

	SuccessOutput struct {
		Message string `json:"message"`
	}

	User struct {
		Gender      string      `json:"gender" db:"gender"`
		Email       string      `json:"email" db:"email"`
		Phone       string      `json:"phone" db:"phone"`
		Cell        string      `json:"cell" db:"cell"`
		Nationality string      `json:"nat" db:"nat"`
		Name        Name        `json:"name" db:"name"`
		Location    Location    `json:"location" db:"location"`
		Login       Login       `json:"login" db:"login"`
		DateOfBirth DateOfBirth `json:"dob" db:"dob"`
		Registered  Registered  `json:"registered" db:"registered"`
		ID          ID          `json:"id" db:"identity"`
		Picture     Picture     `json:"picture" db:"picture"`
	}

	Name struct {
		Title string `json:"title" db:"name_title"`
		First string `json:"first" db:"name_first"`
		Last  string `json:"last" db:"name_last"`
	}

	Location struct {
		Street      Street      `json:"street" db:"street"`
		City        string      `json:"city" db:"city"`
		State       string      `json:"state" db:"state"`
		Country     string      `json:"country" db:"country"`
		Postcode    interface{} `json:"postcode" db:"postcode"`
		Coordinates Coordinates `json:"coordinates" db:"coordinates"`
		Timezone    Timezone    `json:"timezone" db:"timezone"`
	}
	Street struct {
		Number int    `json:"number" db:"number"`
		Name   string `json:"name" db:"name"`
	}

	Coordinates struct {
		Latitude  string `json:"latitude" db:"latitude"`
		Longitude string `json:"longitude" db:"longitude"`
	}

	Timezone struct {
		Offset      string `json:"offset" db:"offset"`
		Description string `json:"description" db:"description"`
	}

	Login struct {
		UUID     string `json:"uuid" db:"uuid"`
		Username string `json:"username" db:"username"`
		Password string `json:"password" db:"password"`
		Salt     string `json:"salt" db:"salt"`
		MD5      string `json:"md5" db:"md5"`
		SHA1     string `json:"sha1" db:"sha1"`
		SHA256   string `json:"sha256" db:"sha256"`
	}

	DateOfBirth struct {
		Date time.Time `json:"date" db:"date"`
		Age  int       `json:"age" db:"age"`
	}

	Registered struct {
		Date time.Time `json:"date" db:"date"`
		Age  int       `json:"age" db:"age"`
	}

	ID struct {
		Name  string `json:"name" db:"name"`
		Value string `json:"value" db:"value"`
	}

	Picture struct {
		Large     string `json:"large" db:"large"`
		Medium    string `json:"medium" db:"medium"`
		Thumbnail string `json:"thumbnail" db:"thumbnail"`
	}
)

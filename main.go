package main

import (
	"os"
	"verba-group-test/pkg/handler"
	"verba-group-test/pkg/repository"
	"verba-group-test/pkg/service"
)

func main() {
	conf := repository.InputConfig{
		Host:     os.Getenv("PG_H"),
		Port:     os.Getenv("PG_P"),
		Username: os.Getenv("PG_U"),
		Password: os.Getenv("PG_W"),
		DBName:   os.Getenv("PG_D"),
		SSLMode:  os.Getenv("PG_S"),
	}

	database, err := repository.NewPostgresDB(conf)
	if err != nil {
		panic(err)
	}

	repos := repository.NewRepository(database)
	services := service.NewService(repos)
	handlers := handler.NewHandler(services)

	server := handlers.GetRouter()

	server.Run()
}
